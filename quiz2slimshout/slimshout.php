<?php

session_cache_limiter(false);
session_start();

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'quiz2slimshout17';
DB::$password = "NFXZCLgsA7EiVXB4";
DB::$dbName = 'quiz2slimshout17';
DB::$encoding = 'utf8';
DB::$port = 3333;
DB::$error_handler = "database_error_handler";
DB::$nonsql_error_handler = "database_error_handler";

function database_error_handler($params) {
    global $app, $log;
    $log->error("SQL Error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query: " . $params['query']);
    }
    $app->render("internal_error.html.twig");
    http_response_code(500);
    die(); // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

\Slim\Route::setDefaultConditions(array(
    'id' => '[1-9][0-9]*',
    'integer' => '(0|-?[1-9][0-9]*)'
));

function getUserIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

$app->get('/', function() use ($app) {
    $app->render('index.html.twig', array('sessionUser' => @$_SESSION['user']));
});

$app->get('/internalerror', function() use ($app, $log) {
    $app->render("internal_error.html.twig");
});

$app->get('/forbidden', function() use ($app) {
    $app->render('forbidden.html.twig');
});

$app->get('/ajax/isusernameregistered/(:username)', function($username = "") use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
    if ($user) {
        echo "username already registered";
    }
});

$app->get('/register', function() use ($app) {
    $app->render('register.html.twig', array('sessionUser' => @$_SESSION['user']));
});

$app->post('/register', function() use ($app, $log) {

    $username = $app->request()->post('username');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    //
    $errorList = array();

    // FIXME: sanitize html tags
    if (strlen($username) < 2 || strlen($username) > 50) {
        array_push($errorList, "Name must be 2-50 characters long");
        $username = "";
    }
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match");
    } else {
        if ((strlen($pass1) < 6) || (preg_match("/[A-Z]/", $pass1) == FALSE ) || (preg_match("/[a-z]/", $pass1) == FALSE ) || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            array_push($errorList, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it");
        }
    }
    $productImage = $_FILES['productImage'];
    // echo "<pre>111\n"; print_r($productImage); //exit;
    if ($productImage['error'] != 0) {
        array_push($errorList, "File submission failed, make sure you've selected an image (1)");
    } else {
        $data = getimagesize($productImage['tmp_name']);
        if ($data == FALSE) {
            array_push($errorList, "File submission failed, make sure you've selected an image (2)");
        } else {
            if (!in_array($data['mime'], array('image/jpeg', 'image/gif', 'image/png'))) {
                array_push($errorList, "File submission failed, make sure you've selected an image (3)");
            } else {
                // FIXME: sanitize file name, otherwise a security hole, maybe
                $productImage['name'] = strtolower($productImage['name']);
                if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $productImage['name'])) {
                    array_push($errorList, "File submission failed, make sure you've selected an image (4)");
                }
                $info = pathinfo($productImage['name']);
                $productImage['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $productImage['name']);
                if (file_exists('uploads/' . $productImage['name'])) {
                    // array_push($errorList, "File submission failed, refusing to override existing file (5)");
                    $num = 1;

                    while (file_exists('uploads/' . $info['filename'] . "_$num." . $info['extension'])) {
                        $num++;
                    }
                    $productImage['name'] = $info['filename'] . "_$num." . $info['extension'];
                }
                // RANDOM NAME INSTEAD OF SANITIZATION
                // $productImage['name'] = RandomString(25) . "." . $info['extension'];
                // all good, nothing to do for now
            }
        }
    }


    if ($errorList) { // STATE 2: failed submission
        $app->render('register.html.twig', array(
            'errorList' => $errorList, 'v' => array('username' => $username)
        ));
    } else { // STATE 3: successful submission
        $imagePath = 'uploads/' . $productImage['name'];
        // DANGERS: // uploads/../slimshop17.php
        // 1. what if name begins with .. and escapes to an upper directory?
        // 2. what if the file extension is dangerous, e.g. php
        // 3. file overriding
        // $log->debug("a $imagePath " . $productImage['tmp_name']);
        if (!move_uploaded_file($productImage['tmp_name'], $imagePath)) {
            $log->err("Error moving uploaded file: " . print_r($productImage, true));
            $app->redirect('/internalerror');
            return;
        }
        DB::insert('users', array('username' => $username, 'password' => $pass1, 'imagePath' => $imagePath));
        $userId = DB::insertId();
        $log->debug("User registed with id=" . $userId);
        $app->render('register_success.html.twig');
    }
});

// STATE 1: first show
$app->get('/login', function() use ($app) {
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app, $log) {
    $username = $app->request()->post('username');
    $password = $app->request()->post('password');

    //
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
    if ($user) {
        if ($user['password'] == $password) {
            $loginSuccessful = true;
        }
    }
    //
    if (!$loginSuccessful) { // array not empty -> errors present
        $log->info(sprintf("Login failed, username=%s, from IP=%s", $username, getUserIpAddr()));
        $app->render('login.html.twig', array('error' => true));
    } else { // STATE 3: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        $log->info(sprintf("Login successful, username=%s, from IP=%s", $username, getUserIpAddr()));
        $app->render('login_success.html.twig', array('sessionUser' => @$_SESSION['user']));
    }
});

$app->get('/logout', function() use ($app) {
    unset($_SESSION['user']);
    $app->render('logout.html.twig');
});

$app->get('/session', function() {
    echo '<pre>';
    print_r($_SESSION);
});


$app->get('/shouts/add', function() use ($app) {
    if (!isset($_SESSION['user'])) {
        $app->redirect('/forbidden');
        return;
    }
    $app->render('shouts_add.html.twig');
});

$app->post('/shouts/add', function() use ($app, $log) {
    if (!isset($_SESSION['user'])) {
        $app->redirect('/forbidden');
        return;
    }
    //
    $authorId = $_SESSION['user']['id']; // ID of currently logged in user
    $message = $app->request()->post('message');

    //
    $errorList = array();
    // FIXME: sanitize html tags in name and description
    if (strlen($message) < 1 || strlen($message) > 100) {
        array_push($errorList, "Name must be 2-100 characters long");
        $message = "";
    }

    //
    if ($errorList) { // STATE 2: failed submission
        $app->render('shouts.html.twig', array('errorList' => $errorList, 'v' => array('message' => $message, 'authorId' => $authorId)));
    } else { // STATE 3: successful submission
        DB::insert('shouts', array('authorId' => $authorId, 'message' => $message));
        $app->render('shouts_add_success.html.twig');
    }
});

$app->get('/shouts/list', function() use ($app) {
    if (!isset($_SESSION['user'])) {
        $app->redirect('/forbidden');
        return;
    }
    $shoutlist = DB::query("SELECT * FROM shouts");
    $userlist = DB::query("SELECT * FROM users");
    $app->render('shouts_list.html.twig', array('shoutlist' => $shoutlist, 'userlist' => $userlist));
});

$app->get('/ajax/loadshouts/by/:username', function($username = "") use ($app, $log) {
    if ($username == "please_choose") {
        $shoutlist = DB::query("SELECT * FROM shouts");
        $app->render('ajax_shouts_page.html.twig', array('shoutlist' => $shoutlist));
    } else {
        $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
        $authorId = $user['id'];
        $shoutlist = DB::query("SELECT * FROM shouts WHERE authorId=%s", $authorId);
        $app->render('ajax_shouts_page.html.twig', array('shoutlist' => $shoutlist));
    }
});


$app->run();

