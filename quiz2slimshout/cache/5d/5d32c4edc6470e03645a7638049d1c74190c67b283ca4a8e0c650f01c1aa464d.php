<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_daaa8e3f38d43a88aceb7cbec84544cac04dad42c075cb0a892867e1277db439 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "slimshout homepage";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "    <div id=\"centerContent\">
        <div>
            
                You can <a href=\"/login\">login</a> or <a href=\"/register\">register</a> or <a href=\"/logout\">logout</a>
            
        </div>
    ";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  46 => 5,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}slimshout homepage{% endblock %}

{% block content %}
    <div id=\"centerContent\">
        <div>
            
                You can <a href=\"/login\">login</a> or <a href=\"/register\">register</a> or <a href=\"/logout\">logout</a>
            
        </div>
    {% endblock content %}
", "index.html.twig", "C:\\xampp\\htdocs\\ipd17\\quiz2slimshout\\templates\\index.html.twig");
    }
}
