<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_a39731b49938c686f3622c12bac7dbedbbf8fe3602ae12819417407c23e6c49a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_headAdd($context, array $blocks = [])
    {
        // line 4
        echo "        <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load(\"/ajax/isemailregistered/\" + email);
                });
            });
        </script>
";
    }

    // line 14
    public function block_content($context, array $blocks = [])
    {
        // line 15
        echo "
    ";
        // line 16
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 17
            echo "        <ul>
            ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 19
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "        </ul>
    ";
        }
        // line 23
        echo "
    <form method=\"post\" enctype=\"multipart/form-data\">
        Email: <input type=\"email\" name=\"email\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", []), "html", null, true);
        echo "\">
        <span class=\"errorMessage\" id=\"isTaken\"></span><br>
        Username: <input type=\"text\" name=\"username\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "username", []), "html", null, true);
        echo "\"><br>
        Password: <input type=\"password\" name=\"pass1\"><br>
        Password (repeated): <input type=\"password\" name=\"pass2\"><br>
        Image: <input type=\"file\" name=\"productImage\">
        <img src=\"/";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "imagePath", []), "html", null, true);
        echo "\" width=\"100\"><br>
        <input type=\"submit\" value=\"Register\">
    </form>

";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 31,  92 => 27,  87 => 25,  83 => 23,  79 => 21,  70 => 19,  66 => 18,  63 => 17,  61 => 16,  58 => 15,  55 => 14,  43 => 4,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block headAdd %}
        <script>
            \$(document).ready(function() {
                \$('input[name=email]').keyup(function() {
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load(\"/ajax/isemailregistered/\" + email);
                });
            });
        </script>
{% endblock headAdd %}

{% block content %}

    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\" enctype=\"multipart/form-data\">
        Email: <input type=\"email\" name=\"email\" value=\"{{v.email}}\">
        <span class=\"errorMessage\" id=\"isTaken\"></span><br>
        Username: <input type=\"text\" name=\"username\" value=\"{{v.username}}\"><br>
        Password: <input type=\"password\" name=\"pass1\"><br>
        Password (repeated): <input type=\"password\" name=\"pass2\"><br>
        Image: <input type=\"file\" name=\"productImage\">
        <img src=\"/{{v.imagePath}}\" width=\"100\"><br>
        <input type=\"submit\" value=\"Register\">
    </form>

{% endblock content %}
", "register.html.twig", "C:\\xampp\\htdocs\\ipd17\\quiz2slimshout\\templates\\register.html.twig");
    }
}
