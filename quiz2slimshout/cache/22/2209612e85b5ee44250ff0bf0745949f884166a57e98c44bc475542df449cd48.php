<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shouts_list.html.twig */
class __TwigTemplate_517fdeba3f334c4bf359105030de8ad6b32bc8823e77f33065f3a3928a953937 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "shouts_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "Shout list";
    }

    // line 5
    public function block_headAdd($context, array $blocks = [])
    {
        // line 6
        echo "    <script>
        \$(document).ready(function () {
            
        });
    </script>
";
    }

    // line 13
    public function block_content($context, array $blocks = [])
    {
        // line 14
        echo "    <p><a href=\"/shouts/add\">add shout</a></p>
    
    <form action=\"/ajax/loadshouts/by/";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "username", []), "html", null, true);
        echo "\" method=\"get\">
        <select >   
            ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usernamelist"]) ? $context["usernamelist"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 19
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", []), "html", null, true);
            echo "\" ";
            echo (($this->getAttribute($context["u"], "selected", [])) ? (" selected=\"selected\" ") : (""));
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", []), "html", null, true);
            echo "</option>      
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "        </select>
        <br>
        <input type=\"submit\" value=\"submit\">
    </form >
    <br>

    <table border=\"1\">
        <tr><th>#</th><th>authorId</th><th>message</tr>
                ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["shoutlist"]) ? $context["shoutlist"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 30
            echo "            <tr>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", []), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "authorId", []), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "message", []), "html", null, true);
            echo "</td>
                
            </tr>                
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "shouts_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 37,  113 => 33,  109 => 32,  105 => 31,  102 => 30,  98 => 29,  88 => 21,  75 => 19,  71 => 18,  66 => 16,  62 => 14,  59 => 13,  50 => 6,  47 => 5,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Shout list{% endblock %}

{% block headAdd %}
    <script>
        \$(document).ready(function () {
            
        });
    </script>
{% endblock headAdd %}

{% block content %}
    <p><a href=\"/shouts/add\">add shout</a></p>
    
    <form action=\"/ajax/loadshouts/by/{{u.username}}\" method=\"get\">
        <select >   
            {% for u in usernamelist %}
                <option value=\"{{u.username}}\" {{ u.selected ? ' selected=\"selected\" ' }}>{{u.username}}</option>      
            {% endfor %}
        </select>
        <br>
        <input type=\"submit\" value=\"submit\">
    </form >
    <br>

    <table border=\"1\">
        <tr><th>#</th><th>authorId</th><th>message</tr>
                {% for p in shoutlist %}
            <tr>
                <td>{{p.id}}</td>
                <td>{{p.authorId}}</td>
                <td>{{p.message}}</td>
                
            </tr>                
        {% endfor %}
    </table>
{% endblock content %}
", "shouts_list.html.twig", "C:\\xampp\\htdocs\\ipd17\\quiz2slimshout\\templates\\shouts_list.html.twig");
    }
}
