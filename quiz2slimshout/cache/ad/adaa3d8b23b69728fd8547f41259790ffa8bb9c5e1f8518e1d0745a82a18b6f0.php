<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ajax_shouts_page.html.twig */
class __TwigTemplate_31308813ee6dcc0a139a93fa15eb583da1232d5452720bde259ac4f29aed1540 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "ajax_shouts_page.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "slimshout";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "<table border=\"1\">
        <tr><th>#</th><th>author id</th><th>message</tr>
                       
                    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["shoutlist"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 10
            echo "                     
                    <tr>
                        <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["s"], "id", []), "html", null, true);
            echo "</td>            
                        <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["s"], "authorId", []), "html", null, true);
            echo "</td>
                        <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["s"], "message", []), "html", null, true);
            echo "</td>
                    </tr> 
             
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "       
</table>
";
    }

    public function getTemplateName()
    {
        return "ajax_shouts_page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 17,  70 => 14,  66 => 13,  62 => 12,  58 => 10,  54 => 9,  49 => 6,  46 => 5,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}slimshout{% endblock %}

{% block content %}
<table border=\"1\">
        <tr><th>#</th><th>author id</th><th>message</tr>
                       
                    {% for s in shoutlist %}
                     
                    <tr>
                        <td>{{s.id}}</td>            
                        <td>{{s.authorId}}</td>
                        <td>{{s.message}}</td>
                    </tr> 
             
            {% endfor %}       
</table>
{% endblock content %}

", "ajax_shouts_page.html.twig", "C:\\xampp\\htdocs\\ipd17\\quiz2slimshout\\templates\\ajax_shouts_page.html.twig");
    }
}
