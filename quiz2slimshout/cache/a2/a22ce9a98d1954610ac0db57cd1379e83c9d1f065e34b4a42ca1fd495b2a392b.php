<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shouts_list.html.twig */
class __TwigTemplate_3a56da19f1f2e09d8fbe86f7529279500b3299ee03b6d74a3acc35ecffbd651d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "shouts_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "Shout list";
    }

    // line 5
    public function block_headAdd($context, array $blocks = [])
    {
        // line 6
        echo "    <script>
        \$(document).ready(function () {
            \$(\"select\").change(function () {
                var username = \$(this).children(\"option:selected\").val();
                alert(\"You have selected  - \" + username);
                window.location=(\"/ajax/loadshouts/by/\" + username);
            });
        });
    </script>
";
    }

    // line 17
    public function block_content($context, array $blocks = [])
    {
        // line 18
        echo "    <p><a href=\"/shouts/add\">add shout</a></p>

    <select >
        <option value=\"please_choose\">--Please choose--</option>
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["userlist"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 23
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", []), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", []), "html", null, true);
            echo "</option>      
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "    </select>
    <br>
     <span class=\"errorMessage\" id=\"isTaken\"></span><br>
    <br>

    <table border=\"1\">
        <tr><th>#</th><th>author name</th><th>author image</th><th>message</tr>
                ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["userlist"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            echo "        
                    ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["shoutlist"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
                // line 34
                echo "                        ";
                if (($this->getAttribute($context["s"], "authorId", []) == $this->getAttribute($context["u"], "id", []))) {
                    // line 35
                    echo "                    <tr>
                        <td>";
                    // line 36
                    echo twig_escape_filter($this->env, $this->getAttribute($context["s"], "id", []), "html", null, true);
                    echo "</td>            
                        <td>";
                    // line 37
                    echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", []), "html", null, true);
                    echo "</td>
                        <td><img src=\"/";
                    // line 38
                    echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "imagePath", []), "html", null, true);
                    echo "\" width=\"50\"></td> 
                        <td>";
                    // line 39
                    echo twig_escape_filter($this->env, $this->getAttribute($context["s"], "message", []), "html", null, true);
                    echo "</td>
                    </tr> 
                ";
                }
                // line 42
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "
    </table>
";
    }

    public function getTemplateName()
    {
        return "shouts_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 44,  136 => 43,  130 => 42,  124 => 39,  120 => 38,  116 => 37,  112 => 36,  109 => 35,  106 => 34,  102 => 33,  96 => 32,  87 => 25,  76 => 23,  72 => 22,  66 => 18,  63 => 17,  50 => 6,  47 => 5,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Shout list{% endblock %}

{% block headAdd %}
    <script>
        \$(document).ready(function () {
            \$(\"select\").change(function () {
                var username = \$(this).children(\"option:selected\").val();
                alert(\"You have selected  - \" + username);
                window.location=(\"/ajax/loadshouts/by/\" + username);
            });
        });
    </script>
{% endblock headAdd %}

{% block content %}
    <p><a href=\"/shouts/add\">add shout</a></p>

    <select >
        <option value=\"please_choose\">--Please choose--</option>
        {% for u in userlist %}
            <option value=\"{{u.username}}\" >{{u.username}}</option>      
        {% endfor %}
    </select>
    <br>
     <span class=\"errorMessage\" id=\"isTaken\"></span><br>
    <br>

    <table border=\"1\">
        <tr><th>#</th><th>author name</th><th>author image</th><th>message</tr>
                {% for u in userlist %}        
                    {% for s in shoutlist %}
                        {% if s.authorId == u.id %}
                    <tr>
                        <td>{{s.id}}</td>            
                        <td>{{u.username}}</td>
                        <td><img src=\"/{{u.imagePath}}\" width=\"50\"></td> 
                        <td>{{s.message}}</td>
                    </tr> 
                {% endif %}
            {% endfor %}
        {% endfor %}

    </table>
{% endblock content %}
", "shouts_list.html.twig", "C:\\xampp\\htdocs\\ipd17\\quiz2slimshout\\templates\\shouts_list.html.twig");
    }
}
