<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_95cc7a385968078320ce42ca371577f432c358a532a7ac4f1b550c564f6b453a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo "Sign In ";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        // line 6
        echo "
    <div id=\"centerContent\">
        ";
        // line 8
        if (((isset($context["fbUser"]) ? $context["fbUser"] : null) || (isset($context["user"]) ? $context["user"] : null))) {
            // line 9
            echo "            <h1>\"You are already logged in\"</h1>
            <a  href=\"/\">Logout</a> \"OR\"
            <a  href=\"/\">Continue</a>
        ";
        } else {
            // line 13
            echo "            
            ";
            // line 14
            if ((isset($context["loginForm"]) ? $context["loginForm"] : null)) {
                // line 15
                echo "                <h1>\"You are now logged In\"</h1>
                <a href=\"/\">Continue</a>
            ";
            } else {
                // line 18
                echo "                
                <div >
                    ";
                // line 20
                if ((isset($context["loginFailed"]) ? $context["loginFailed"] : null)) {
                    // line 21
                    echo "                        <h4 style=\"color:red; font-size: 12px;\">Login Failed</h4>
                    ";
                }
                // line 23
                echo "
                    <form action=\"/login\">
                        <h2 >Sign In</h2>
                        Email: <input type=\"email\" name=\"email\" class=\"form-control\" aria-describedby=\"basic-addon1\" required>     
                        <br/>
                        Password: <input type=\"password\" name=\"password\" class=\"form-control\" aria-describedby=\"basic-addon3\" required >

                        <br/>
                        <button  type=\"submit\">Sign In</button>
                    </form>
                    <br>
                    <a href=\"";
                // line 34
                echo twig_escape_filter($this->env, (isset($context["loginUrl"]) ? $context["loginUrl"] : null), "html", null, true);
                echo "\"><img src=\"/images/facebook.png\"></a>
                    
                    
                </div>
            ";
            }
            // line 39
            echo "        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 39,  94 => 34,  81 => 23,  77 => 21,  75 => 20,  71 => 18,  66 => 15,  64 => 14,  61 => 13,  55 => 9,  53 => 8,  49 => 6,  46 => 5,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Sign In {% endblock %}

{% block content %}

    <div id=\"centerContent\">
        {% if fbUser or user %}
            <h1>\"You are already logged in\"</h1>
            <a  href=\"/\">Logout</a> \"OR\"
            <a  href=\"/\">Continue</a>
        {% else %}
            
            {% if loginForm %}
                <h1>\"You are now logged In\"</h1>
                <a href=\"/\">Continue</a>
            {% else %}
                
                <div >
                    {% if loginFailed %}
                        <h4 style=\"color:red; font-size: 12px;\">Login Failed</h4>
                    {% endif %}

                    <form action=\"/login\">
                        <h2 >Sign In</h2>
                        Email: <input type=\"email\" name=\"email\" class=\"form-control\" aria-describedby=\"basic-addon1\" required>     
                        <br/>
                        Password: <input type=\"password\" name=\"password\" class=\"form-control\" aria-describedby=\"basic-addon3\" required >

                        <br/>
                        <button  type=\"submit\">Sign In</button>
                    </form>
                    <br>
                    <a href=\"{{loginUrl}}\"><img src=\"/images/facebook.png\"></a>
                    
                    
                </div>
            {% endif %}
        </div>
    {% endif %}
{% endblock %}

", "login.html.twig", "C:\\xampp\\htdocs\\ipd17\\fb-login\\templates\\login.html.twig");
    }
}
