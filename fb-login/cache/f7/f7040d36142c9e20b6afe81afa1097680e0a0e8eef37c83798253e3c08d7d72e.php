<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_39d5cfe2db36f8d6bc7e2aca5feddf67e2d0798d90cf73891e92387fa518706a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        echo " E-Shop";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        echo "   
 
";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        // line 10
        echo "    <div id=\"centerContent\">
        <h1>todo</h1>
        your can <a href=\"/login\">Log in</a> or <a href=\"/logout\">Log out</a>
    </div>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 10,  55 => 9,  47 => 5,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %} E-Shop{% endblock %}

{% block head %}   
 
{% endblock %}

{% block content %}
    <div id=\"centerContent\">
        <h1>todo</h1>
        your can <a href=\"/login\">Log in</a> or <a href=\"/logout\">Log out</a>
    </div>
{% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\ipd17\\fb-login\\templates\\index.html.twig");
    }
}
