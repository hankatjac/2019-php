<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_249f6e0fba3f8cae2cad3c04604e2723a9aa029d6d573e7a9c4bc7be3dd2eb5d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
        <!-- Styles -->
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 8
        $this->displayBlock('head', $context, $blocks);
        // line 10
        echo "    </head>
    <body>
  ";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        // line 14
        echo "    </body>
</html>
";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
    }

    // line 8
    public function block_head($context, array $blocks = [])
    {
        // line 9
        echo "        ";
    }

    // line 12
    public function block_content($context, array $blocks = [])
    {
        // line 13
        echo "        ";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  74 => 13,  71 => 12,  67 => 9,  64 => 8,  59 => 7,  53 => 14,  51 => 12,  47 => 10,  45 => 8,  41 => 7,  33 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>
        <!-- Styles -->
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>{% block title %}{% endblock %}</title>
        {% block head %}
        {% endblock %}
    </head>
    <body>
  {% block content %}
        {% endblock %}
    </body>
</html>
", "master.html.twig", "C:\\xampp\\htdocs\\fb-login\\templates\\master.html.twig");
    }
}
