<?php

require_once 'vendor/autoload.php';
session_start();

//Prevent browser caching
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

//wzBf4gzm7r7twiot

if ($_SERVER['SERVER_NAME'] == 'localhost') {

    DB::$dbName = 'ecommerce';
    DB::$user = 'root';
    DB::$password = '';
    DB::$host = 'localhost:3333'; // sometimes needed on Mac OSX
} else { // hosted on external server
    require_once 'fbauth.php';
    DB::$dbName = 'cp4724_fastfood-online';
    DB::$user = 'cp4724_fastfood';
    DB::$password = '[^)EJ;Fw%402';
}

DB::$encoding = 'utf8'; // defaults to latin1 if omitted
DB::$error_handler = 'sql_error_handler';
DB::$nonsql_error_handler = 'nonsql_error_handler';

function nonsql_error_handler($params) {
    global $app, $log;
    $log->error("Database error: " . $params['error']);
    http_response_code(500);
    $app->render('internal_error.html.twig');
    die;
}

function sql_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("in query: " . $params['query']);
    http_response_code(500);
    $app->render('error_internal.html.twig');
    die; // don't want to keep going if a query broke
}

$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();

$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');


if ($_SERVER['SERVER_NAME'] != 'localhost') {
//sessions and Cookies
    $helper = $fb->getRedirectLoginHelper();
    $permissions = ['public_profile', 'email', 'user_location']; // optional
    $loginUrl = $helper->getLoginUrl('http://look.ipd17.com/fblogin-callback.php', $permissions);
    $logoutUrl = $helper->getLoginUrl('http://look.ipd17.com/fblogout-callback.php', $permissions);
}
if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}
if (!isset($_SESSION['facebook_access_token'])) {
    $_SESSION['facebook_access_token'] = array();
}

$twig = $app->view()->getEnvironment();
if ($_SERVER['SERVER_NAME'] != 'localhost') {
    $twig->addGlobal('fbUser', $_SESSION['facebook_access_token']);
    $twig->addGlobal('loginUrl', $loginUrl);
}
$twig->addGlobal('user', $_SESSION['user']);


//FIXME: VAlidate all parameters
\Slim\Route::setDefaultConditions(array(
    'ID' => '\d+',
    'slug' => '[A-Za-z0-9-]+',
    'pageNum' => '\d+',
));

//Handler for the home page
$app->get('/', function() use ($app, $log) {
    //if a fb user than check id does already have a record in the users table, 
    if ($_SESSION['facebook_access_token']) {
        $userID = DB::queryFirstField('SELECT ID from users WHERE fbID = %s', $_SESSION['facebook_access_token']['ID']);
        if (!$userID) {
            $result = DB::insert('users', array(
                        'fbID' => $_SESSION['facebook_access_token']['ID'],
            ));
            if ($result) {
                $userID = DB::insertId();
                $log->debug(sprintf("Regisetred fbUsere %s with id %s", $_SESSION['facebook_access_token']['ID'], $userID));
                $_SESSION['facebook_access_token']['userID'] = $userID;
            } else {
                //add the userId to the fbUser session for convenience
                $log->debug(sprintf("Failed to register fbUsere %d", $_SESSION['facebook_access_token']['ID']));
                $_SESSION['facebook_access_token'] = null;
                $app->render('fblogin_failed.html.twig');
            }
        } else {
            $_SESSION['facebook_access_token']['userID'] = $userID;
        }
    }
    $app->render('index.html.twig');
});

// State 1: first show
$app->get('/login', function() use ($app, $log) {
    require_once 'fbauth.php';
    $app->render('login.html.twig', array('loginUrl' => $loginUrl));
});
// State 2: submission
$app->post('/login', function() use ($app, $log) {
    $email = $app->request->post('email');
    $password = $app->request->post('password');
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s and locked=false", $email);
    if (!$user) {
        $log->debug(sprintf("User failed for email %s from IP %s", $email, $_SERVER['REMOTE_ADDR']));
        $app->render('login.html.twig', array('loginFailed' => TRUE));
    } else {
        // password MUST be compared in PHP because SQL is case-insenstive
        if (password_verify($password, $user['password'])) {
            // LOGIN successful
            unset($user['password']);
            $_SESSION['user'] = $user;
            $_SESSION['facebook_access_token'] = array();
            $log->debug(sprintf("User %s logged in successfuly from IP %s", $user['ID'], $_SERVER['REMOTE_ADDR']));
            $app->render('login.html.twig', array('loginForm' => TRUE));
        } else {
            $log->debug(sprintf("User failed for email %s from IP %s", $email, $_SERVER['REMOTE_ADDR']));
            $app->render('login.html.twig', array('loginFailed' => TRUE));
        }
    }
});

$app->get('/logout', function() use ($app, $log) {
    $_SESSION['user'] = array();
    $_SESSION['facebook_access_token'] = array();
    $app->render('logout_success.html.twig');
});

$app->run();
