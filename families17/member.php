<!DOCTYPE html>
<html>
    <head>
        <title>member</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div id="centerContent">
            <?php
            require_once 'db.php';

            $headId = isset($_GET['id']) ? $_GET['id'] : -1;
            $result = mysqli_query($link, sprintf("SELECT * FROM members WHERE headId='%s'", mysqli_real_escape_string($link, $headId)));
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            $member = mysqli_fetch_assoc($result);
            if ($member) {
                echo "<table border=1>\n";
                echo "<tr><th>#</th><th>headId</th><th>Name</th></tr>\n";

                while ($member = mysqli_fetch_assoc($result)) {
                    echo "<tr><td>" . $member['id'] . "</td>";
                    echo "<td><a href=memberedit.php?id=" . $member['headId'] . ">".$member['headId']."</a></td>";
                    echo "<td>" . $member['name'] . "</td>";
                    echo "</tr>\n";
                }
                echo "</table>\n\n";
            } else { // 404 - not found
                http_response_code(404);
                echo "<p>404 - member not found <a href=index.php>click to continue</a></p>";
            }
            ?>
            <p>To get back to index<a href="index.php">click here</a></p>
        </div>
    </body>
</html>



