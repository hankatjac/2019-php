<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
          require_once 'db.php';
            if (isset($_SESSION['user'])) {
                $user = $_SESSION['user'];
                echo "<p>You are logged in as " .$user['username'] .
                        ". You may <a href=articleadd.php>post a new article</a>."
                        . " or <a href=logout.php>log out</a>.</p>";
            } else {
                echo "<p>You are NOT logged in. You may <a href=login.php>log in</a>"
                . " or <a href=register.php>register</a></p>";
            }
            //
            $result = mysqli_query($link, "SELECT heads.id, familyName FROM heads");
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            
            while ($member = mysqli_fetch_assoc($result)) {
                
                echo "<a href=member.php?id=".$member['id']."><h1>" . $member['familyName'] . "</h1></a>\n";
              
            }
        ?>
    </body>
</html>
