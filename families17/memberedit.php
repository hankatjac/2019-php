<!DOCTYPE html>
<html>
    <head>
        <title>Edit article</title>
        <link rel="stylesheet" href="styles.css">
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>

    </head>
    <body>
        <div id="centerContent">
            <?php
            require_once 'db.php';

// only allow access if user is logged in
            if (!isset($_SESSION['user'])) {
                echo '<p>Access denied: you must be <a href="login.php">logged in</a> to access this page</p>';
                exit;
            }
            $headId = isset($_GET['id']) ? $_GET['id'] : -1;
            
            if (!($_SESSION['user']['id']=$headId)){
                
                echo '<p>Access denied: you must be family head to access this page</p>';
                exit;
            }
                    

// here-document or "here-doc"
            function getForm($nameVal = "") {

                $form = <<< ENDMARKER
<form method="post">
      
        Name: <input type="text" name="name" value="$nameVal"><br>
        <input type="submit" value="Add member">
</form>
ENDMARKER;
                return $form;
            }

// are we receiving form submission?
            // are we receiving form submission?
            if (isset($_POST['name'])) {
                $name = $_POST['name'];
                $errorList = array();
                //
                if (strlen($name) < 1 || strlen($name) > 50) {
                    array_push($errorList, "Title must be 1-50 characters long");
                }

                if ($errorList) { // array not empty -> errors present
                    // STATE 2: Failed submission
                    echo "<p>There were problems with your submission:</p>\n<ul>\n";
                    foreach ($errorList as $error) {
                        echo "<li class=\"errorMessage\">$error</li>\n";
                    }
                    echo "</ul>\n";
                    echo getForm($name);
                } else {
                    // STATE 3: Successful submission
                    echo "<p>member updated successfully</p>";
                    echo '<p><a href="index.php">Click here to continue</a></p>';
                    //
                    
                    $result = mysqli_query($link, sprintf("UPDATE members SET name='%s' WHERE headId='%s'", 
                            mysqli_real_escape_string($link, $name), 
                            mysqli_real_escape_string($link, $headId)));
                    if (!$result) {
                        echo "SQL Query failed: " . mysqli_error($link);
                        exit;
                    }
                }
            } else {
                // STATE 1: First show    
                $result = mysqli_query($link, sprintf("SELECT * FROM members WHERE headId='%s'", mysqli_real_escape_string($link, $headId)));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                $member = mysqli_fetch_assoc($result);
                if ($member) {
                    echo getForm($member['name']);
                } else { // 404 - not found
                    http_response_code(404);
                    echo "<p>404 - member not found <a href=index.php>click to continue</a></p>";
                }
            }
            ?>
        </div>
    </body>
</html>

