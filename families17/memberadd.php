<!DOCTYPE html>
<html>
    <head>
        <title>Add article</title>
        <link rel="stylesheet" href="styles.css">
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
        <script>tinymce.init({selector:'textarea'});</script>
    </head>
<body>
    <div id="centerContent">
<?php

require_once 'db.php';

// only allow access if user is logged in
if (!isset($_SESSION['user'])) {
    echo '<p>Access denied: you must be <a href="login.php">logged in</a> to access this page</p>';
    exit;
}

// here-document or "here-doc"
function getForm($nameVal = "") { 
   
$form = <<< ENDMARKER
<form method="post">

        Name: <input type="text" name="name" value="$nameVal"><br>
        <input type="submit" value="Add member">
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['name'])) {
   
    $name = $_POST['name'];
    $errorList = array();
    //
    if (strlen($name) < 5 || strlen($name) > 200) {
        array_push($errorList, "Title must be 5-200 characters long");
    }
  
 
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name);
    } else {
        // STATE 3: Successful submission
        echo "<p>Article added successfully</p>";
        echo '<p><a href="index.php">Click here to continue</a></p>';
        //
        $headId = $_SESSION['user']['id']; // ID of currently logged in user
        $result = mysqli_query($link, sprintf("INSERT INTO members VALUES (NULL, '%s', '%s')",
            mysqli_real_escape_string($link, $headId),
            mysqli_real_escape_string($link, $name)
          
                ));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>

