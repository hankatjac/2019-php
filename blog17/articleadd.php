<!DOCTYPE html>
<html>
<head>
    <title>Add article</title>
    <link rel="stylesheet" href ="styles.css" >
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
    <script>tinymce.init({selector:'textarea'});</script>
</head>

<body>
    <div id="centerContent">
<?php

require_once 'db.php';
if (!isset($_SESSION['user'])){
    echo '<p>Access denied</p>';
    exit;
}

// here-document or "here-doc"
function getForm($titleVal="", $bodyVal=""){
$form = <<< ENDMARKER
<form method="post">
    Title:<input type ="text" name="title" value="$titleVal">
    <textarea cols=60 rows=30 name= "body">$bodyVal</textarea>   
    <input type="submit" value ="Add article">
</form>
ENDMARKER;
return $form;
}

// are we receiving from submission
if (isset($_POST['title'])){
    $title= $_POST['title'];
    $body = $_POST['body'];
    $errorList = array();
    if (strlen($title)<10 || strlen($title) >200) {
        array_push($errorList, "Title must be 10-200 characters long");
    }
     if (strlen($body) < 5 || strlen($body) > 65000) {
        array_push($errorList, "Body must be 5-65000 characters long");
    }


    if ($errorList){ // array not empty -> errors present
    // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p><ul>\n";
        foreach ($errorList as $error){
            echo "<li class=\"errorMessage\">$error</li>";
        }
        echo "</ul>\n";
        echo getForm($title, $body);
    } else{
        echo "<p>article add successfully</p>"; 
        echo '<p><a href="index.php">Click here to continue</a></p>';
        $authorId = $_SESSION['user']['id'];
                  $result = mysqli_query($link, sprintf("INSERT INTO articles VALUES (NULL, '%s', NULL,'%s','%s')",
                         mysqli_real_escape_string($link, $authorId),
                         mysqli_real_escape_string($link, $title),
                         mysqli_real_escape_string($link, $body)));
                 if (!$result) {
                     echo "SQL Query failed: " . mysqli_error($link);
                     exit;
                 } 
    }

}else { 
//state 1 :first show 
   echo getForm();
}
?>
</div>
</body>
</html>

