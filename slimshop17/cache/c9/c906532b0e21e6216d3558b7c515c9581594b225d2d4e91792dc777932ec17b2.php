<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_a39731b49938c686f3622c12bac7dbedbbf8fe3602ae12819417407c23e6c49a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_headAdd($context, array $blocks = [])
    {
        // line 3
        echo "        
   <script>
    \$(document).ready(function(){
       \$('input[name = email]').keyup(function(){
           var email = \$('input[name = email]').val();
           \$('#isTaken').load(\"/isemailregistered/\" + email);
       });
    });
    </script>

";
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        // line 16
        echo "
    ";
        // line 17
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 18
            echo "        <ul>
            ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 20
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </ul>
    ";
        }
        // line 24
        echo "
    <form method=\"post\">
        Email: <input type=\"email\" name=\"email\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", []), "html", null, true);
        echo "\"><br>
        <span class=\"errorMessage\" id=\"isTaken\"></span><br>
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", []), "html", null, true);
        echo "\"><br>
        Password: <input type=\"password\" name=\"pass1\"><br>
        Password (repeated): <input type=\"password\" name=\"pass2\"><br>
   
        <input type=\"submit\" value=\"Register\">
    </form>

";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 28,  89 => 26,  85 => 24,  81 => 22,  72 => 20,  68 => 19,  65 => 18,  63 => 17,  60 => 16,  57 => 15,  43 => 3,  40 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
 {% block headAdd %}
        
   <script>
    \$(document).ready(function(){
       \$('input[name = email]').keyup(function(){
           var email = \$('input[name = email]').val();
           \$('#isTaken').load(\"/isemailregistered/\" + email);
       });
    });
    </script>

{% endblock %}

{% block content %}

    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\">
        Email: <input type=\"email\" name=\"email\" value=\"{{v.email}}\"><br>
        <span class=\"errorMessage\" id=\"isTaken\"></span><br>
        Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br>
        Password: <input type=\"password\" name=\"pass1\"><br>
        Password (repeated): <input type=\"password\" name=\"pass2\"><br>
   
        <input type=\"submit\" value=\"Register\">
    </form>

{% endblock content %}
", "register.html.twig", "C:\\xampp\\htdocs\\ipd17\\slimshop17\\templates\\register.html.twig");
    }
}
