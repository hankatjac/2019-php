<!DOCTYPE html>
<html>
    <head>
        <title>people list</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body>

<?php
require_once 'db.php';

$result = mysqli_query($link, sprintf("SELECT * FROM people"))or die(mysqli_error($link));
if ($result->num_rows > 0) {
    // output data of each row
    
    
  echo " <table>\n";
  echo "<tr>
    <th>id</th>
    <th>name</th> 
    <th>gpa</th>
    <th>isGraduate</th>
    <th>gender</th>
    </tr>\n";
  

//    while($row = $result->fetch_assoc()) {
    while($person = mysqli_fetch_assoc($result)) {
        echo "<tr><td>" . $person['id'] . "</td>";
        echo "<td>" . $person['name'] . "</td>";
        echo "<td>" . $person['gpa'] . "</td>";
        echo "<td>" . ($person['isGraduate']=='true' ? "graduate" : "undergraduate") . "</td>";
        echo "<td>" . $person['gender'] . "</td></tr>\n";
    }
} else {
    echo "0 results";
}

//mysqli_close($link);
?> 

</body>
</html>