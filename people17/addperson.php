<!DOCTYPE html>
<html>
    <head>
        <title>Add person</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body >
    <div >
<?php

require_once 'db.php';

// only allow access if user is logged in


// here-document or "here-doc"
function getForm($nameVal = "", $gpaVal = "", $isGraduateVal = false, $genderVal= "male") {    
//    $isGradChecked = $isGraduateVal ? 'checked' : '';
//    $rbMaleChecked = $rbFemaleChecked = $rbOtherChecked = "";
//    switch ($genderVal) {
//        case "male": $rbMaleChecked = 'checked'; break;
//        case "female": $rbFemaleChecked = 'checked'; break;
//        case "other": $rbOtherChecked = 'checked'; break;
//        default: // $rbOtherChecked = 'checked';
//            // FIXME: maybe display error message here
//            die("Error: gender value submitted is unrecognized.");
//    }
$form = <<< ENDMARKER
<form method="post" id="centerContent">
    Name: <input type="text" name="name" value="$nameVal"><br>
        
    GPA: <input type="number" step= 0.01 name="gpa" value="$gpaVal"><br>
        
    <label for="checkbox">Is graduate</label>    
    <input type="checkbox" name="isGraduate" value="true" checked /><br>    
    gender:    
    <input type="radio" name="gender" id="gender_f" value="Female" />
    <label for="gender_f">Female</label>
    
    <input type="radio" name="gender" id="gender_m" value="male" />
    <label for="gender_m">Male</label>
    
    <input type="radio" name="gender" id="gender_u" value="other" />
    <label for="gender_u">other</label>
    <br>
    <input type="submit" value="Add person">
             
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['name'])) {
    $name= $_POST['name'];
    $gpa = $_POST['gpa'];
    $isGraduate = isset($_POST['isGraduate']);
    $gender = $_POST['gender'];
    $errorList = array();
    //
    if (strlen($name) < 1 || strlen($name) > 50) {
        array_push($errorList, "Title must be 1-50 characters long");
    }
    if (strlen($gpa) < 0 || strlen($gpa) > 4.3) {
        array_push($errorList, "GPA must be 0-4.3 characters long");
    }
    //
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name, $gpa, $isGraduate, $gender);
    } else {
        // STATE 3: Successful submission
        echo "<p>person added successfully</p>";
       
        
       // ID of currently logged in user
        $result = mysqli_query($link, sprintf("INSERT INTO people VALUES (NULL, '%s', '%s', '%s','%s')",
            mysqli_real_escape_string($link, $name),
            mysqli_real_escape_string($link, $gpa),
            mysqli_real_escape_string($link, $isGraduate? "true": "false"),
            mysqli_real_escape_string($link, $gender)  
                
                ));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }  
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>



