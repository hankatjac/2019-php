-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jun 17, 2019 at 04:52 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz1shout`
--

-- --------------------------------------------------------

--
-- Table structure for table `shouts`
--

CREATE TABLE `shouts` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `message` varchar(100) NOT NULL,
  `tsPosted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shouts`
--

INSERT INTO `shouts` (`id`, `name`, `message`, `tsPosted`) VALUES
(1, 'Alabama', 'Anyone?', '2019-06-17 13:59:04'),
(2, 'Alabama', 'I am here', '2019-06-17 14:05:49'),
(3, 'Alabama', 'I am here', '2019-06-17 14:06:14'),
(4, 'Alabama', 'I am here', '2019-06-17 14:10:44'),
(5, 'Alabama', 'I am here', '2019-06-17 14:11:01'),
(6, 'Jerry', 'I am here', '2019-06-17 14:12:39'),
(7, 'Jerry', 'Hi everyone!', '2019-06-17 14:35:48'),
(8, 'Jerry', 'How are you!', '2019-06-17 14:47:49'),
(9, 'Jerry', 'Come here', '2019-06-17 14:48:20'),
(10, 'Jerry', 'Go to school.', '2019-06-17 14:49:42'),
(11, 'William', 'What\'s day today?', '2019-06-17 14:50:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shouts`
--
ALTER TABLE `shouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shouts`
--
ALTER TABLE `shouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
