<!DOCTYPE html>
<html>
    <head>
        <title>user</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div id="centerContent">
            <?php
            require_once 'db.php';

            $username = isset($_GET['user']) ? $_GET['user'] : null;
            if ($username) {
                $result = mysqli_query($link, sprintf("SELECT * FROM shouts WHERE name='%s'", mysqli_real_escape_string($link, $username)));
                if (!$result) {
                    echo "SQL Query failed: " . mysqli_error($link);
                    exit;
                }
                $shout = mysqli_fetch_assoc($result);
                if ($shout) {
                    while ($shout = mysqli_fetch_assoc($result)) {
                        echo "<ul>\n";
                        echo "<li>" . $shout['id'] ." ". $shout['name'] . " ". $shout['message'] . " ". $shout['tsPosted'] . "</li>\n";
                        echo "</ul>\n\n";
                    }
                } else { // 404 - not found
                    http_response_code(404);
                    echo "<p>404 - no record found <a href=index.php>click to continue</a></p>";
                }
            } else {
                echo "<p>please proved user name.</p>";
            }
            ?>
<!--            <p>To get back to index<a href="register.php">click here</a></p>-->
        </div>
    </body>
</html>



