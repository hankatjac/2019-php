<!DOCTYPE html>
<html>
    <head>
        <title>Add car</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div id="centerContent">
            <?php
            require_once 'db.php';

            function getForm($nameVal = "", $messageVal = "") {
// here-document or "here-doc"    
                $form = <<< ENDMARKER
<form method="post">
    Name: <input type="text" name="name" value="$nameVal"><br>    
    Message: <input type="text" name="message" value="$messageVal"><br>   
   <br>
    <input type="submit" name ="submit" value="Shout">
</form>
ENDMARKER;
                return $form;
            }

// are we receiving form submission?
            if (isset($_POST['name'])) {
                $name = $_POST['name'];
                $message = $_POST['message'];
                $errorList = array();
                // TODO: verify values

                if (preg_match('/^[a-zA-Z0-9_]{2,20}$/', $name) != 1) {
                    array_push($errorList, "Plates must be 1-10 characters long and only "
                            . "consist of uppercase/lowercase letters, digits, and underscores");
                    $username = "";
                }

                if (strlen($message) < 1 || strlen($message) > 100) {
                    array_push($errorList, " message must be 1-20 characters long");
                    $message = "";
                }

                if ($errorList) { // array not empty -> errors present
                    // STATE 2: Failed submission
                    echo "<p>There were problems with your submission:</p>\n<ul>\n";
                    foreach ($errorList as $error) {
                        echo "<li class=\"errorMessage\">$error</li>\n";
                    }
                    echo "</ul>\n";
                    echo getForm($name, $message);
                } else {
                    // STATE 3: Successful submission
                    $result = mysqli_query($link, sprintf("INSERT INTO shouts VALUES (NULL, '%s','%s', NULL)", mysqli_real_escape_string($link, $name), mysqli_real_escape_string($link, $message)
                    ));
                    if (!$result) {
                        echo "SQL Query failed: " . mysqli_error($link);
                        exit;
                    }

                    echo "<p>shout added successfully</p>";

                    if (!isset($_SESSION['count'])) {
                        $_SESSION['count'] = 0;
                    }
                    $_SESSION['count']++;
                    echo "<p>You've shouted this page " . $_SESSION['count'] . " times</p>";
                }
            } else {
                // STATE 1: First show
                echo getForm();
            }

            $result = mysqli_query($link, "SELECT * FROM shouts ORDER BY tsPosted DESC LIMIT 10");
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }

            while ($shout = mysqli_fetch_assoc($result)) {
                echo "<p>* on" . $shout['tsPosted'] . $shout['name'] . "shouted : " . $shout['message'] . "</p>";
            }
            ?>
        </div>
    </body>
</html>


