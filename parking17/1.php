<?php

require_once 'db.php';

$res = mysqli_query($link, sprintf("SELECT * FROM owners"));
if (!$res) {
    echo "SQL Query failed: " . mysqli_error($link);
    exit;
} 

echo "OwnerId: " . "<select name=\"ownerId\">";
    
while ($row = mysqli_fetch_array($res))
{ 
     echo "<option value=\"$row[0]\">$row[0]</option>";
}        
echo "</select>";