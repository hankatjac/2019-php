<!DOCTYPE html>
<html>
    <head>
        <title>Add car</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div id="centerContent">
<?php

//require_once 'db.php';
//
//$res = mysqli_query($link, sprintf("SELECT * FROM owners"));
//if (!$res) {
//    echo "SQL Query failed: " . mysqli_error($link);
//    exit;
//} 
//
//echo "OwnerId: " . "<select name=\"ownerId\">";
//    
//while ($row = mysqli_fetch_array($res))
//{ 
//     echo "<option value=\"$row[0]\">$row[0]</option>";
//}        
//echo "</select>";

ob_start();
include '1.php';
$include = ob_get_clean();


function getForm($makeModelVal="", $platesVal="", $engineSizeLVal="") {
    global $include;
// here-document or "here-doc"    
$form = <<< ENDMARKER
<form method="post">
    $include;
    Make Model: <input type="text" name="makeModel" value="$makeModelVal"><br>    
    Plates: <input type="text" name="plates" value="$platesVal"><br>  
    Engine Size (L): <input type="number" step = "0.01" name="engineSizeL" value="$engineSizeLVal"><br>
    Fuel Type: <input type="radio" name="fuelType" value="gasolin" >gasoline</input>
    <input type="radio" name="fuelType" value="disel" >disel</input>
    <input type="radio" name="fuelType" value="electric" >electric</input>
    <input type="radio" name="fuelType" value="hybrid" >hybrid</input>
    <input type="radio" name="fuelType" value="eco" >eco</input>    
   <br>
    <input type="submit" value="register">
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['ownerId'])) {
    $ownerId = $_POST['ownerId'];
    $makeModel = $_POST['makeModel'];
    $plates = $_POST['plates'];
    $engineSizeL = $_POST['engineSizeL'];
    $fuelType = $_POST['fuelType'];
    $errorList = array();
    // TODO: verify values
    if (strlen($makeModel) < 1 || strlen($makeModel) > 50) {
        array_push($errorList, "Make-model  must be 1-50 characters long");
        $name = "";
    }
    if (preg_match('/^[a-zA-Z0-9_]{1,10}$/', $plates) != 1) {
      array_push($errorList, "Plates must be 1-10 characters long and only "
              . "consist of uppercase/lowercase letters, digits, and underscores");
      $username = "";
    }
     if (!is_numeric($engineSizeL) || $engineSizeL < 0 || $engineSizeL > 99.99) {
        array_push($errorList, "GPA must be a number 0 to 4.3");
        $engineSizeL = "";
    }
    //
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($makeModel, $plates, $engineSizeL);
    } else {
        // STATE 3: Successful submission
        $result = mysqli_query($link, sprintf("INSERT INTO cars VALUES (NULL, '%s','%s','%s','%s','%s')",
            mysqli_real_escape_string($link, $ownerId),
            mysqli_real_escape_string($link, $makeModel),    
            mysqli_real_escape_string($link, $plates),    
            mysqli_real_escape_string($link, $engineSizeL),
            mysqli_real_escape_string($link, $fuelType)   
            ));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "<p>Car added successfully</p>";
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>


