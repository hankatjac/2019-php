<!DOCTYPE html>
<html>
    <head>
        <title>Car list</title>
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>
        <div id="centerContent">
            <?php
            require_once 'db.php';
            $result = mysqli_query($link, "SELECT * FROM cars, owners WHERE cars.ownerId= owners.id");
            if (!$result) {
                echo "SQL Query failed: " . mysqli_error($link);
                exit;
            }
            echo "<table border=1>\n";
            echo "<tr><th>#</th><th>make model</th><th>plates</th><th>engine Size</th><th>fuel type</th><th>ower name</th></tr>\n";
            while ($car = mysqli_fetch_assoc($result)) {
                echo "<tr><td>" . $car['id'] . "</td>";
                
                echo "<td>" . $car['makeModel'] . "</td>";
                echo "<td>" . $car['plates'] . "</td>";
                echo "<td>" . $car['engineSizeL'] . "</td>";
                echo "<td>" . $car['fuelType']. "</td>";
                echo "<td>" . $car['name'] . "</td>"
                        . "</tr>\n";
            }
            echo "</table>\n\n";
            ?>
        </div>
    </body>
</html>

