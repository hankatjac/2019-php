<!DOCTYPE html>
<html>
    <head>
        <title>Add owner</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div id="centerContent">
<?php

require_once 'db.php';

// here-document or "here-doc"
function getForm($nameVal = "") {
   
 
$form = <<< ENDMARKER
<form method="post">
    Name: <input type="text" name="name" value="$nameVal"><br><br>
    <input type="submit" value="register">
</form>
ENDMARKER;
return $form;
}

// are we receiving form submission?
if (isset($_POST['name'])) {
    $name = $_POST['name'];
    
    $errorList = array();
    // TODO: verify values
    if (strlen($name) < 1 || strlen($name) > 50) {
        array_push($errorList, "Name must be 1-50 characters long");
        $name = "";
    }

    //
    if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name);
    } else {
        // STATE 3: Successful submission
        $result = mysqli_query($link, sprintf("INSERT INTO owners VALUES (NULL, '%s')",
            mysqli_real_escape_string($link, $name)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "<p>Owner added successfully</p>";
    }
} else { 
    // STATE 1: First show
    echo getForm();
}

?>
    </div>
</body>
</html>


